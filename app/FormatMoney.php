<?php

namespace App;

/**
 * Class FormatMoney
 * @package App
 */
class FormatMoney
{

    const NUMS = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen' ];

    const TENS = [ '', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety' ];

    /**
     * @param $num
     * @return string
     * @throws \Exception
     */
    function numberToMoney($num) :string
    {
        if (!is_numeric($num)) {
            throw new \Exception('Input Number should be numeric');
        }

        $num = (float)$num;

        if ($num < 0 || $num > 1000 ) {
            throw new \Exception('Input Number should between 0 to 1000');
        }

        if ( $num * 100 != floor($num * 100)) {
            throw new \Exception('Input Number should not have more than 2 decimals');
        }

        $centsNumber = ($num * 100) % 100 ;
        $dollars = $this->numberToWords($num) . ' dollars';
        $cents = $this->numberToWords($centsNumber) . ' cents';
        switch (true) {
            case $centsNumber === 0:
                return $dollars;
            case $centsNumber > 0 && $num < 1:
                return $cents;
            default:
                return $dollars . ' and ' . $cents;

        }
    }

    /**
     * @param $num
     * @return string
     */
    public function numberToWords($num) :string
    {
        $digit = $num%10;
        switch (true) {
            case $num < 20:
                return self::NUMS[$num];
            case $num < 100:
                return self::TENS[floor($num / 10)] . ($digit ? " " . self::NUMS[$digit]: "");
            case $num < 1000:
                return self::NUMS[floor($num / 100)] ." hundred and" . ($num % 100 === 0? "": " " . $this->numberToWords($num % 100));
            default:
                return 'one thousand';
        }
    }
}