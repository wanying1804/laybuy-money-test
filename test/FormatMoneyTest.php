<?php

use PHPUnit\Framework\TestCase;
use App\FormatMoney;

class FormatMoneyTest extends TestCase {

    private $formatMoney;

    protected function setUp(){
        $this->formatMoney = new FormatMoney();
    }

    /**
     * @dataProvider  formattedMoney
     */
    public function testFormatMoney($num, $expected){
        $this->assertEquals($this->formatMoney->numberToMoney($num), $expected);
    }

    /**
     * @dataProvider  exceptions
     */
    public function testException($num){
        $this->expectException(Exception::class);
        $this->formatMoney->numberToMoney($num);
    }

    public function formattedMoney(){
        return [
            ['0',     "zero dollars"],
            ['0.12',  "twelve cents"],
            [10.55, "ten dollars and fifty five cents"],
            [120,   "one hundred and twenty dollars"]
        ];
    }

    public function exceptions()
    {
        return[
            [-1],
            [1001],
            [100.232],
            ['xxx']
        ];
    }

}